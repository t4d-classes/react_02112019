# Welcome to the React Class

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Wednesday, 9am to 4:30pm PST

Breaks:

- Morning: 10:25am to 10:35am
- Lunch: Noon to 1pm
- Afternoon #1: 2:05pm to 2:15pm
- Afternoon #2: 3:15pm to 3:25pm

## Course Outline

- Day 1 - What is React, Functional Components, JSX, Props, Default Props, Class Components, State, State Hook
- Day 2 - Composition (including containment + specialization)
- Day 3 - Lifecycle Functions (including Effect Hook), Keys, Refs (including Ref Hook), Unit Testing

### Requirements

- Node.js (version 10 or later)
- Web Browser
- Text Editor

### Instructor's Resources

- [DevelopIntelligence](http://www.developintelligence.com/)
