Exercise 2

1. Add a table under the header in Car Tool. Please use a table tag and related tags to build the table.

2. The table should have 6 columns. The columns are as follows:

id
make
model
year
color
price

Each column should have a header

3. Display 2 rows of car data, populating each field. Please display more than 1 row, but more than 2 is not needed

4. Ensure it loads correctly on the screen