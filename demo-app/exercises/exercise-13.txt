Exercise 13

1. When the page initially loads, set the focus on the model field of the Car Form component.

2. When you click 'Edit', set the focus on the year field of the edit row.

3. Ensure it works.