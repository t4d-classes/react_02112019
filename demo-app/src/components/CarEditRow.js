import React, { useState, useRef, useEffect } from 'react';

export const CarEditRow = ({ car, onSaveCar, onCancelCar }) => {

  const [ carForm, setCarForm ] = useState({
    make: car.make,
    model: car.model,
    year: car.year,
    color: car.color,
    price: car.price,
  });

  const makeInputRef = useRef(null);

  useEffect(() => {
      if (makeInputRef.current) {
            makeInputRef.current.focus();
      }
  }, []);

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" id="make-input" value={carForm.make}
          onChange={e => setCarForm({ ...carForm, make: e.target.value })} ref={makeInputRef} /></td>
    <td><input type="text" id="model-input" value={carForm.model}
          onChange={e => setCarForm({ ...carForm, model: e.target.value })} /></td>
    <td><input type="number" id="year-input" value={carForm.year}
          onChange={e => setCarForm({ ...carForm, year: Number(e.target.value) })} /></td>
    <td><input type="text" id="color-input" value={carForm.color}
          onChange={e => setCarForm({ ...carForm, color: e.target.value })} /></td>
    <td><input type="number" id="price-input" value={carForm.price}
          onChange={e => setCarForm({ ...carForm, price: Number(e.target.value) })} /></td>
    <td>
      <button type="button" onClick={() => onSaveCar({ ...carForm, id: car.id })}>Save</button>
      <button type="button" onClick={onCancelCar}>Cancel</button>
    </td>
  </tr>;
};