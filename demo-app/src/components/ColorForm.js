import React, { useState, useRef, useEffect } from 'react';

export const ColorForm = (props) => {

  // object destructuring
  const { buttonText, onSubmitColor } = props;

  // array destructuring
  const [ newColor, setNewColor ] = useState('');
  const [ test, setTest ] = useState('');

  const newColorInputRef = useRef(null);

  // useEffect(() => {
  //   if (newColorInputRef.current) {
  //     console.log('setting focus');
  //     console.log('test changed ', test);
  //     newColorInputRef.current.focus();
  //   }
  // }, [ test ]);

  // const newColorStateHook = useState('');
  // const newColor = newColorStateHook[0];
  // const setNewColor = newColorStateHook[1];


  const submitColor = () => {
    onSubmitColor(newColor);
    setNewColor('');
  };

  return <form>
    <div>
      <label htmlFor="new-color-input">New Color:</label>
      <input type="text" id="new-color-input" value={newColor}
        onChange={e => setNewColor(e.target.value)} ref={newColorInputRef} />
    </div>
    <div>
      <label htmlFor="test-input">Test:</label>
      <input type="text" id="test-input" value={test}
        onChange={e => setTest(e.target.value)} />
    </div>
    <button type="button" onClick={submitColor}>{buttonText}</button>
  </form>;

};