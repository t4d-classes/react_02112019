import React from 'react';
import ReactDOM from 'react-dom';

import { ColorTool } from './components/ColorTool';
import { CarTool } from './components/CarTool';

const colorList = ['blue', 'black', 'red', 'green'];

const carList = [
  { id: 1, make: 'Ford', model: 'F-150', year: 1980, color: 'green', price: 2000, },
  { id: 2, make: 'Ford', model: 'F-250', year: 1990, color: 'blue', price: 2000, },
];

ReactDOM.render(
  <div>
    <ColorTool colors={colorList} />
    <CarTool cars={carList} />
  </div>,
  document.querySelector('#root'),
);
